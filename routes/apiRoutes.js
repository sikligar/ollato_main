import express from 'express';
const router = express.Router();

import * as Users from '../controllers/UserController';
import * as UserProfile from '../controllers/UserProfileController';
import * as ListingDetails from '../controllers/ListingDetailsController';
import * as validateLoginInput from '../controllers/validations/login';

const auth = require('../middlewares/authenticate');
// var user = require('../controllers/UserController');

router.route('/login')
	.post((req,res) => {
		Users.login(req,res);
	});
router.route('/register')
	.post((req,res) => {
		
		Users.register(req,res);
	});
router.route('/forgot-password')
	.post((req,res) => {
		
		Users.forgotPassword(req,res);
	})
router.route('/user-profile')
	.post(auth,(req,res) => {
		UserProfile.storeProfile(req,res);
	})

router.route('/user-profile')
	.get(auth,(req,res) => {
		UserProfile.getProfile(req,res);
	})
router.route('/state')
	.get(auth,(req,res) => {

		ListingDetails.getState(req,res);
	})
router.route('/grades')
	.get(auth,(req,res) => {

		ListingDetails.getGrade(req,res);
	})
router.route('/school')
	.get(auth,(req,res) => {

		ListingDetails.getSchool(req,res);
	})
export default router;
	