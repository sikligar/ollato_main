import HttpStatus from 'http-status-codes';
import jwt from 'jsonwebtoken';
import KvtmLogin from '../models/KvtmLogin';
const key = require("../config/application").key;
/**
 * Route authentication middleware to verify a token
 *
 * @param {object} req
 * @param {object} res
 * @param {function} next
 *
 */

module.exports = (req, res, next) => {
    const authorizationHeader = req.headers['authorization'];
    if(!authorizationHeader){

        var errMsg = 'Token is not provided';
        res.status(200).json({
            success:false, 
            message: errMsg, 
        })
        return;
    }
    let token;

    if (authorizationHeader) {
        token = authorizationHeader.split(' ')[1];
    }
    if (token) {
        jwt.verify(token, key, (err, decoded) => {
            if (err) {
                var errMsg = 'You are not authorized to perform this operation!';
                res.status(200).json({
                    success:false, 
                    message: errMsg, 
                })
                return;
                // res.status(HttpStatus.UNAUTHORIZED).json({error: 'You are not authorized to perform this operation!'});
            } else {
                KvtmLogin.where('User_Id',decoded.id).fetch().then(user => {
                    if (!user) {

                        var errMsg = 'No user found';
                        res.status(200).json({
                            success:false, 
                            message: errMsg, 
                        });
                        return;
                        // res.status(HttpStatus.NOT_FOUND).json({error: 'No such user'});
                    } else {
                        req.user_id = user.attributes.User_Id;
                        next();
                    }

                });
            }
        });
    } else {

        var errMsg = 'No token provided';
        res.status(200).json({
            success:false, 
            message: errMsg, 
        })
        // res.status(HttpStatus.FORBIDDEN).json({
        //     error: 'No token provided'
        // });
    }
};