import bookshelf from '../config/bookshelf';

const TABLE_NAME = 'kvmt_login';

/**
 * Shop model.
 */
class KvtmLogin extends bookshelf.Model {

    /**
     * Get table name.
     */
    get tableName() {
        return TABLE_NAME;
    }

    /**
     * Table has timestamps.
     */
    get hasTimestamps() {
        return true;
    }
}

export default KvtmLogin;