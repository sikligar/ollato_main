import bookshelf from '../config/bookshelf';

const TABLE_NAME = 'kvmt_school';

/**
 * Shop model.
 */
class KvmtSchool extends bookshelf.Model {

    /**
     * Get table name.
     */
    get tableName() {
        return TABLE_NAME;
    }

    /**
     * Table has timestamps.
     */
    get hasTimestamps() {
        return true;
    }
}

export default KvmtSchool;