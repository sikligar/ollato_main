import bookshelf from '../config/bookshelf';

const TABLE_NAME = 'kvmt_student';

/**
 * Shop model.
 */
class KvmtStudent extends bookshelf.Model {

    /**
     * Get table name.
     */
    get tableName() {
        return TABLE_NAME;
    }

    /**
     * Table has timestamps.
     */
    get hasTimestamps() {
        return true;
    }
}

export default KvmtStudent;