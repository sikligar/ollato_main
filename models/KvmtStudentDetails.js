import bookshelf from '../config/bookshelf';

const TABLE_NAME = 'kvmt_student_det';

/**
 * Shop model.
 */
class KvmtStudentDetails extends bookshelf.Model {

    /**
     * Get table name.
     */
    get tableName() {
        return TABLE_NAME;
    }

    /**
     * Table has timestamps.
     */
    get hasTimestamps() {
        return true;
    }

    school_details (){
        
        return this.hasMany(Products, 'id','product_id');
    }
}

export default KvmtStudentDetails;