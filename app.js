import path from 'path';
import app from './config/express';
import express from 'express';
import routes from './routes/apiRoutes';
import swagger from './config/swagger';
import * as errorHandler from './middlewares/errorHandler';
import joiErrorHandler from './middlewares/joiErrorHandler';
import expressValidator from 'express-validator';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpackConfig from './webpack/webpack.config.dev';






// if (process.env.NODE_ENV === 'development') {

//     const compiler = webpack(webpackConfig);
//     app.use(webpackDevMiddleware(compiler, {noInfo: true, publicPath: webpackConfig.output.publicPath}));
//     app.use(webpackHotMiddleware(compiler));
// }

app.get('/swagger.json', (req, res) => {
   res.json(swagger);
});

// Joi Error Handler
// app.use(expressValidator);
app.use('/api', routes);
// app.use(joiErrorHandler);
app.use(express.static(path.join(__dirname + '/public')));
app.use(joiErrorHandler);

// Error Handler
app.use(errorHandler.notFoundErrorHandler);
app.use(errorHandler.genericErrorHandler);
app.use(errorHandler.methodNotAllowed);

app.listen(app.get('port'), app.get('host'), () => {
    console.log(`Server running at http://${app.get('host')}:${app.get('port')}`);
});

export default app;
