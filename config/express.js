import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';
import helmet from 'helmet';
import compression from 'compression';
import methodOverride from 'method-override';

import constant from '../config/directory';
import expressValidator from 'express-validator';
// var session = require('express-session');
var session = require('client-sessions');
var cookieParser = require('cookie-parser');
//added for shopify app
import crypto from 'crypto';
import cookie from 'cookie';
const nonce = require('nonce')();
import querystring from 'querystring';

const app = express();

require('dotenv').config();

app.set('port',  process.env.APP_PORT || 3000);
app.set('host',  process.env.HOST || 'localhost');

app.use(express.json());
app.use(express.static(constant.distDir));

app.use(cors());
app.use(helmet());
app.use(compression());
app.use(methodOverride());
app.use(bodyParser.urlencoded({extended: true,limit: '50mb'}));
app.use(bodyParser.json());


// app.use(expressValidator);
app.use(morgan('dev'));
app.use(express.static(constant.assetsDir));
app.use(cookieParser());

// var expressValidator = require('express-validator');
// app.use(expressValidator)

app.use(session({
  cookieName: 'session',
  secret: 'random_string_goes_here',
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
}));


export default app;