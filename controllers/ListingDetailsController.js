import KvmtState from '../models/KvmtState';
import KvmtSchool from '../models/KvmtSchool';
import KvmtGrade from '../models/KvmtGrade';


exports.getState = function(req,res){


	KvmtState.fetchAll().then(function(state){


		var stateModel = state.models;
		var stateDetail = [];
		// console.log(stateModel);
		stateModel.forEach(function (model) { 

			stateDetail.push(model.attributes.state);
		})
		// console.log(stateDetail);
		res.status(200).json({
	        success:true, 
	        data: stateDetail, 
        })
	});
};
exports.getGrade = function(req,res){

	KvmtGrade.fetchAll().then(function(state){


		var gradeModel = state.models;
		var gradeDetail = [];
		// console.log(gradeModel);
		gradeModel.forEach(function (model) { 

			gradeDetail.push(model.attributes.Grade_Desc);
		})
		// console.log(gradeDetail);
		res.status(200).json({
	        success:true, 
	        data: gradeDetail, 
        })
	});
}
exports.getSchool = function(req,res){

	KvmtSchool.fetchAll().then(function(state){


		var schoolModel = state.models;
		var schoolDetail = [];
		
		schoolModel.forEach(function (model) { 

			schoolDetail.push(model.attributes.School_Name);
		})
		// console.log(schoolDetail);
		res.status(200).json({
	        success:true, 
	        data: schoolDetail, 
        })
	});
}