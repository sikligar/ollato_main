import KvtmLogin from '../models/KvtmLogin';
import KvmtStudent from '../models/KvmtStudent';
import KvmtState from '../models/KvmtState';
import KvmtSchool from '../models/KvmtSchool';
import KvmtGrade from '../models/KvmtGrade';
import KvmtStudentDetails from '../models/KvmtStudentDetails';
import logger from '../config/winston';
import Joi from '@hapi/joi';

const fs = require('fs');
const fileType = require('file-type');
const AppPath = require("../config/constant");

exports.storeProfile = function(req,res){

	const { body } = req;
    const emailSchema = Joi.object().keys({

        name: Joi.string().required(),
        dob : Joi.string().required(),
		gender : Joi.string().required(),
		address : Joi.string().required(),
		email : Joi.string().email().required(),
		mobile : Joi.string().min(10).max(10).required(),
		state : Joi.string().required(),
		city : Joi.string().required(),
		schoolname : Joi.string().required(),
		board : Joi.string().required(),
		standard : Joi.string().required(),
		profile : Joi.string().required(),
    });
    const result = Joi.validate(body,emailSchema);

    const { value, error } = result; 
    const valid = error == null; 

    if (!valid) {

        const { details } = error; 
        const message = details.map(i => i.message).join(',');
        const messages = message.split('"').join('');
        const errMsg = messages.charAt(0).toUpperCase()+messages.slice(1);
        res.status(200).json({
            success:false, 
            message: errMsg, 
        })
        return;
    }

    KvmtState.where({'state':req.body.state}).fetch().then(function(state){

        if(!state){

            var errMsg = 'State not found'
            res.status(200).json({
                success:false, 
                message: errMsg, 
            })
            return;
        }
        KvmtSchool.where({'School_Name':req.body.schoolname}).fetch().then(function(schoolDetails){
            
            if(!schoolDetails){

                var errMsg = 'School name not found'
                res.status(200).json({
                    success:false, 
                    message: errMsg, 
                })
                return;
            }

            KvmtGrade.where({'Grade_Desc':req.body.standard}).fetch().then(function(studentGrade){
                // console.log(studentGrade);
                if(!studentGrade){

                    var errMsg = 'Standard not found';
                    res.status(200).json({
                        success:false, 
                        message: errMsg, 
                    })
                    return;
                }



                var profile = req.body.profile;
                var split = profile.split(',');
                let buff = Buffer.from(split[1], 'base64');
                let mimeinfo = fileType(Buffer.from(split[1], 'base64'));
                // console.log('mimeinfo');
                // console.log(mimeinfo);
                if(mimeinfo.ext != 'png' && mimeinfo.ext != 'jpg' && mimeinfo.ext != 'jpeg'){

                    var errMsg = 'Please upload image only'
                    res.status(200).json({
                        success:false, 
                        message: errMsg, 
                    })
                    return;
                }
                
                var document_name = Date.now() +'.'+mimeinfo.ext;
                var filename = './public/backend/profile-images/'+document_name;

                fs.writeFile(filename, buff,function(error){


                    var grade = studentGrade.attributes.Grade_Id;
                    var school_id = schoolDetails.attributes.School_Id;
                    var state_id = state.attributes.state_id;
                    
                    var today = new Date();
                    var dd = String(today.getDate()).padStart(2, '0');
                    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                    var yyyy = today.getFullYear();

                    today = yyyy + '-' + mm + '/' + dd;

                    var editJson = {
                        Student_Name: req.body.name,
                        St_Dob: req.body.dob,
                        St_Gender: req.body.gender,
                        St_Mobile: req.body.mobile,
                        St_Email: req.body.email,
                        St_Address_1: req.body.address,
                        St_City: req.body.city,
                        st_state: state_id,
                        profile : document_name,
                        Updated_at: today,
                    };

                    
                    KvmtStudent.where({User_Id:req.user_id})
                    .save(editJson,{patch:true})
                    .then(function(x) {
                    
                        KvmtStudent.where({User_Id:req.user_id}).fetch().then(function(studentData){
                            
                            var student_id = studentData.attributes.Student_Id;
                            var studentDetails = {
                                St_School_Id:school_id,
                                Grade_Id:grade,
                                St_Board:req.body.board,
                                Updated_at: today,
                            }

                            KvmtStudentDetails.where('Student_Id',student_id).count().then(function(count){

                                if(count > 0){

                                    KvmtStudentDetails.where({Student_Id:student_id})
                                    .save(studentDetails,{patch:true})
                                    .then(function(savedData){

                                        var success = 'Your profile updated successfully';
                                        res.status(200).json({
                                            success:true, 
                                            message: success, 
                                        })
                                        return;
                                    })
                                }else{

                                    var addStudentDet = new KvmtStudentDetails({
                                        Student_Id:student_id,
                                        St_School_Id:school_id,
                                        St_Board:req.body.board,
                                        Grade_Id:grade,
                                    }).save().then(registerData => {

                                        return res.json({
                                          success: true,
                                          message:'Register Successfully',
                                        });

                                    })
                                }
                            })
                        })
                    })
                })

            })
        })

    })
}

exports.getProfile =  function(req,res){
    // console.log('hi');
    var user_id  = req.user_id;


    var name = '';
    var dob = '';
    var gender = '';
    var address = '';
    var email = '';
    var state = '';
    var city = '';
    var mobile = '';
    var schoolname = '';
    var board = '';
    var standard = '';
    var profile = '';
    KvmtStudent.where({User_Id:user_id}).fetch().then(function(studentData){
        
        var studentId = studentData.attributes.Student_Id;
        name = studentData.attributes.Student_Name;
        dob = studentData.attributes.St_Dob;
        gender = studentData.attributes.St_Gender;
        mobile = studentData.attributes.St_Mobile;
        email = studentData.attributes.St_Email;
        address = studentData.attributes.St_Address_1;
        city = studentData.attributes.St_City;
        profile = AppPath.profile_path +studentData.attributes.profile;

        
        if(studentData.attributes.st_state != '' && studentData.attributes.st_state != null){

            KvmtState.where('state_id',studentData.attributes.st_state).fetch().then(function(stateData){

                state = stateData.attributes.state


                KvmtStudentDetails.where('Student_Id',studentId).count().then(function(count){

                    if(count > 0){

                        KvmtStudentDetails.where({Student_Id:studentId}).fetch().then(function(studentDetails){

                                board = studentDetails.attributes.St_Board;
                            
                                KvmtSchool.where({School_Id:studentDetails.attributes.St_School_Id}).fetch().then(function(school){

                                    schoolname = school.attributes.School_Name;
                                    
                                     KvmtGrade.where({Grade_Id:studentDetails.attributes.Grade_Id}).fetch().then(function(grade){

                                        standard = grade.attributes.Grade_Desc;
                                        
                                        
                                        var result = {

                                            name        : name,
                                            dob         :  dob,
                                            gender      : gender,
                                            address     : address,
                                            email       : email,
                                            state       : state,
                                            city        :  city,
                                            mobile      : mobile,
                                            schoolname  : schoolname,
                                            board       : board,
                                            standard    : standard,
                                            profile     : profile,
                                        }

                                        return res.json({
                                            success: true,
                                            data:result,
                                        });

                                    })
                                })
                            
                        });
                    }
                    
                })
            })
        }else{

            var result = {

                name        : name,
                dob         :  dob,
                gender      : gender,
                address     : address,
                email       : email,
                state       : state,
                city        :  city,
                mobile      : mobile,
                schoolname  : schoolname,
                board       : board,
                standard    : standard,
                profile    : profile,
            }

            return res.json({
                success: true,
                data:result,
            });

        }

        
       
    })   
}