const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateRegisterInput(data) {
  let errors = {};

    data.username = !isEmpty(data.username) ? data.username : "";
    data.email = !isEmpty(data.email) ? data.email : "";
    data.password = !isEmpty(data.password) ? data.password : "";

    if (!Validator.isEmail(data.email)) {
        errors.email = "Email is invalid";
    }
    if (Validator.isEmpty(data.username)) {
        errors.username = "Don't forget to add your username.";
    }

    if (Validator.isEmpty(data.password)) {
        errors.password = "The password you entered is incorrect.";
    }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
