const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateLoginInput(data) {
  let errors = {};

    data.username = !isEmpty(data.username) ? data.username : "";
    data.password = !isEmpty(data.password) ? data.password : "";

    
    if (Validator.isEmpty(data.username)) {
        errors.username = "Don't forget to add your username.";
    }

    if (Validator.isEmpty(data.password)) {
        errors.password = "The password you entered is incorrect.";
    }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
