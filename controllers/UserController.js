import HttpStatus from 'http-status-codes';
import jwt from 'jsonwebtoken';
const bcrypt = require("bcryptjs");
import KvtmLogin from '../models/KvtmLogin';
import KvmtStudent from '../models/KvmtStudent';
import logger from '../config/winston';
import Joi from '@hapi/joi';
const validateLoginInput = require("./validations/login");
const validateRegisterInput = require("./validations/register");
const validateForgotPassword = require("./validations/forgotPassword");
const key = require("../config/application").key;
const nodemailer = require("nodemailer");
const email = require("../config/mail");


exports.login = function(req,res){
  	
  	// const { errors, isValid } = validateLoginInput(req.body); // Finds the validation errors in this request and wraps them in an object with handy functions
  	
  	// if (!isValid) {
   //      return res.status(422).json(errors);
   //  }

   	const { body } = req;
    const emailSchema = Joi.object().keys({

        username: Joi.string().required(),
        password: Joi.string().required(),
    });
    const result = Joi.validate(body,emailSchema);

    const { value, error } = result; 
    const valid = error == null; 

    if (!valid) {

        const { details } = error; 
        const message = details.map(i => i.message).join(',');
        const messages = message.split('"').join('');
        const errMsg = messages.charAt(0).toUpperCase()+messages.slice(1);
        res.status(200).json({
            success:false, 
            message: errMsg, 
        })
        return;
    }

    try{
    	// const errors = '';
    	var login = KvtmLogin.where({User_Name:req.body.username}).fetch().then(loginData =>{

    		if(!loginData){

    			var errMsg = "The username you entered does not belong to any account.";
            	// return res.status(404).json(errors);
          	res.status(200).json({
	            success:false, 
	            message: errMsg, 
	          })
	          return;
    		}
    		var bodyPassword = req.body.password;
    		var password = loginData.attributes.Password;
    		
    		// Check Password
	        bcrypt.compare(bodyPassword, password).then(isMatch => {
	        	
	        	if (isMatch) {
                  
	            	// User Matched
	                const payload = { id: loginData.attributes.User_Id, username: loginData.attributes.User_Name}; // Create JWT Payload
                  // console.log(payload);
	                // Sign Token
	                jwt.sign(payload, key, { expiresIn: 36000 }, (err, token) => {
	                    return res.json({
	                        success: true,
	                        message:'Login Successfully',
	                        token: "Bearer " + token
	                    });
	                });
	            } else {

	            	var errMsg = "The password you entered is incorrect.";
	            	res.status(200).json({
			            success:false, 
			            message: errMsg, 
                })
			        return;
	            }
	        });
    	});

    }catch(e){

    	console.log('no data found');
    }
};
exports.register = function(req,res){

  	/*const { errors, isValid } = validateRegisterInput(req.body); // Finds the validation errors in this request and wraps them in an object with handy functions

  	if (!isValid) {
  		// console.log(errors);
  		return res.status(404).json(errors);
  	}*/

  	const { body } = req;
    const registerSchema = Joi.object().keys({

        username: Joi.string().required(),
        password: Joi.string().required(),
        email: Joi.string().email().required(),
    });
    const result = Joi.validate(body,registerSchema);

    const { value, error } = result; 
    const valid = error == null; 

    if (!valid) {

        const { details } = error; 
        const message = details.map(i => i.message).join(',');
        const messages = message.split('"').join('');
        const errMsg = messages.charAt(0).toUpperCase()+messages.slice(1);
        res.status(200).json({
            success:false, 
            message: errMsg, 
        })
        return;
    }

  	var username = req.body.username;
  	var email = req.body.email;
  	var password = req.body.password;

  	KvtmLogin.where('User_Name',username).count().then(function(count){
  		if(count > 0){

  			var errMsg = 'Username is already exists';
  			res.status(200).json({
	            success:false, 
	            message: errMsg, 
	        })
	        return;
  			// errors.username = "Username is already exists";
  			// return res.status(404).json(errors);
  		}
      KvmtStudent.where('St_Email',email).count().then(function(counts){

        if(counts > 0){

          var errMsg = 'Email address is already exists';
          res.status(200).json({
                success:false, 
                message: errMsg, 
            })
            return;
          
        }
  	  	bcrypt.genSalt(10,(err,salt) => {

  	  		bcrypt.hash(password,salt,(err, hash) => {
  	  			if (err) throw err;

  	  			password = hash;

  	  			var register = new KvtmLogin({
  	  				Role_Id:40,
  	  				User_Name:username,
  	  				Password:password,
  	  				User_Type:2,
  	  				Is_Active:'Y',
  	  				Created_By:'Admin',
  	  				Modified_By:'Admin'
  	  			}).save().then(user => {


  	  				
  	  				var addEmail = new KvmtStudent({
  	  					User_Id:user.id,
  	  					St_Email:email,
  	  					Created_By:'Admin',
  	  					Modified_By:'Admin'
  	  				}).save().then(registerData => {

  	  					return res.json({
                  success: true,
                  message:'Register Successfully',
                });

  	  				})
  	  			});
  	  		})
  	  	})
      })
  	})
}


exports.forgotPassword = function(req,res){

	var emailId = req.body.email;

	/*const { errors, isValid } = validateForgotPassword(req.body); // Finds the validation errors in this request and wraps them in an object with handy functions

  	if (!isValid) {
  		// console.log(errors);
  		return res.status(404).json(errors);
  	}*/

  	const { body } = req;
    const registerSchema = Joi.object().keys({

        email: Joi.string().email().required(),
    });
    const result = Joi.validate(body,registerSchema);

    const { value, error } = result; 
    const valid = error == null; 

    if (!valid) {

        const { details } = error; 
        const message = details.map(i => i.message).join(',');
        const messages = message.split('"').join('');
        const errMsg = messages.charAt(0).toUpperCase()+messages.slice(1);
        res.status(200).json({
            success:false, 
            message: errMsg, 
        })
        return;
    }

  	KvmtStudent.where('St_Email',emailId).count().then(function(count){

  		if(count < 1){

  			var errMsg = 'Email address not found';	
  			res.status(200).json({
		        success:false, 
		        message: errMsg, 
		    })
  			// return res.status(404).json(errors);
  		}

  		bcrypt.hash('123456', 10, function(err, hash) {
			
			// console.log(hash);
			KvmtStudent.where({'St_Email':emailId}).fetch().then(function(student){
				// console.log(student.attributes.User_Id);
				var userId = student.attributes.User_Id;
				KvtmLogin.where({User_Id:userId})
                    .save({Password: hash},{patch:true})
                    .then(function(x) {

                    })
			})
		});
		const transport = nodemailer.createTransport({
		  	host: email.host,
		  	port: email.port,
		  	auth: {
		    	user: email.username,
		    	pass: email.password
		  	}
		});
		// console.log(transport);
		const mailOptions = {
		    from: 'noreply@thinktanker.in',
		    to: emailId,
		    subject: 'Forgot Password',
		    html: '<p>Your new password is '+123456+'</p>'
		};
		// console.log(mailOptions);
		transport.sendMail(mailOptions,function(err,info){
		    if(err){
		      console.log(err);
		    }else{
		    	
		    	return res.json({
                    success: true,
                    message:'Password is send in your email',
                }); 
		    }
		});
  	})

}